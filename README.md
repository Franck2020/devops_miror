# devops-42_2020

Back :
Php - Laravel
Port - 8000
Config: ./back/.env.example || ./back/config
requirements : PhP >= 7.1, Composer

Database :
Mysql - default port = 3306 - default username 'root' - default password 'root' / '' -> to edit in back .env
requirements : PhP >= 7.1

front :
Javascript - angular
Port - default = 4200
Config : ./front/src/environments/
requirements : npm

please push on "develop" :)